package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.presenter.ipresenter.IForgotPasswordPresenter;

public interface IForgotPasswordView extends IView<IForgotPasswordPresenter> {
        void goToOneTimePassword();
}
