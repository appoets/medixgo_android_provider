package com.boiddo.android.provider.view.activity;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import com.boiddo.android.provider.R;
import com.boiddo.android.provider.presenter.OneTimePasswordPresenter;
import com.boiddo.android.provider.presenter.ipresenter.IOneTimePasswordPresenter;
import com.boiddo.android.provider.view.iview.IOneTimePasswordView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

import static com.boiddo.android.provider.ZtoidApplication.getApplicationInstance;

public class OneTimePasswordActivity extends BaseActivity<IOneTimePasswordPresenter> implements IOneTimePasswordView {

    @BindView(R.id.etOTP)
    EditText etOTP;

    @Override
    int attachLayout() {
        return R.layout.activity_onetime_password;
    }

    @Override
    IOneTimePasswordPresenter initialize() {
        return new OneTimePasswordPresenter(this);
    }

    @Override
    public void setUp() {
        String OTP = getApplicationInstance().getOTP();
        etOTP.setText(OTP);
    }

    @OnClick({R.id.btnSendOTP})
    public void OnClickView(View view){
        switch (view.getId()){
            case R.id.btnSendOTP:
                validateOTP();
                break;
        }
    }

    private void validateOTP() {
        String otp = etOTP.getText().toString().trim();
        String receivedOTP = getApplicationInstance().getOTP();
        if (otp.equalsIgnoreCase("")){
            showSnackBar(getString(R.string.please_enter_otp));
        }else if(!Objects.equals(otp,receivedOTP)){
            showSnackBar(getString(R.string.otp_not_matched));
        }else{
            iPresenter.goToForgotChangePassword();
        }
    }

    @Override
    public void goToForgotChangePassword() {
            startActivity(new Intent(this,ForgotChangePasswordActivity.class));
    }
}
