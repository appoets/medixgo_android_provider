package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.model.dto.common.HistoryItem;
import com.boiddo.android.provider.presenter.ipresenter.IHistoryPresenter;
import com.boiddo.android.provider.view.adapter.HistoryAdapter;

public interface IHistoryView extends IView<IHistoryPresenter> {
    void setAdapter(HistoryAdapter adapter);
    void initSetUp();
    void moveToChat(HistoryItem data);
}
