package com.boiddo.android.provider.model;

import com.boiddo.android.provider.model.dto.request.ScheduleRequest;
import com.boiddo.android.provider.model.dto.response.ScheduleResponse;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.model.webservice.ApiClient;
import com.boiddo.android.provider.model.webservice.ApiInterface;

public class ScheduleModel extends BaseModel<ScheduleResponse> {

    public ScheduleModel(IModelListener<ScheduleResponse> listener) {
        super(listener);
    }

    public void updateSchedule(ScheduleRequest request) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).schedule(request));
    }

    @Override
    public void onSuccessfulApi(ScheduleResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
