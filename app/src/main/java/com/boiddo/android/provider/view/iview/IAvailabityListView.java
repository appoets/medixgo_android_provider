package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.model.dto.response.AvailabilityListResponse;
import com.boiddo.android.provider.presenter.ipresenter.IAvailabilityListPresenter;
import com.boiddo.android.provider.view.adapter.listener.IAvailabilityListListener;

import java.util.List;

public interface IAvailabityListView extends IView<IAvailabilityListPresenter> {
    void setAdapter(List<AvailabilityListResponse> list);
    void moveToDetail(AvailabilityListResponse data);
    void initSetUp();
}
