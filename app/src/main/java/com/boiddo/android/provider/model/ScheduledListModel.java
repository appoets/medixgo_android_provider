package com.boiddo.android.provider.model;


import com.boiddo.android.provider.model.dto.response.ScheduledListResponse;
import com.boiddo.android.provider.model.listener.IModelListListener;
import com.boiddo.android.provider.model.webservice.ApiClient;
import com.boiddo.android.provider.model.webservice.ApiInterface;

import java.util.List;

public class ScheduledListModel extends BaseListModel<ScheduledListResponse> {

    public ScheduledListModel(IModelListListener<ScheduledListResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(ScheduledListResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<ScheduledListResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getScheduleList() {
        enQueueListTask(new ApiClient().getClient().create(ApiInterface.class).getScheduledList());
    }
}
