package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.model.dto.response.ScheduledListResponse;
import com.boiddo.android.provider.presenter.ipresenter.IScheduledListPresenter;
import com.boiddo.android.provider.view.adapter.listener.IScheduledListListener;

import java.util.List;

public interface IScheduledListView extends IView<IScheduledListPresenter> {
    void setAdapter(List<ScheduledListResponse> list, IScheduledListListener iScheduledListListener);
    void moveToDetail(ScheduledListResponse data);
    void initSetUp();
}
