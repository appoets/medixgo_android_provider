package com.boiddo.android.provider.presenter.ipresenter;

public interface IOneTimePasswordPresenter extends IPresenter {
    void goToForgotChangePassword();
}