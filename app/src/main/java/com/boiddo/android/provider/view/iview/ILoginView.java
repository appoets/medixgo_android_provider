package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.presenter.ipresenter.ILoginPresenter;

public interface ILoginView extends IView<ILoginPresenter> {
    void goToRegistration();
    void goToForgotPassword();
    void goToHome();
}
