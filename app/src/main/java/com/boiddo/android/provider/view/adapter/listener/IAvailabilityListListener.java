package com.boiddo.android.provider.view.adapter.listener;


import com.boiddo.android.provider.model.dto.response.AvailabilityListResponse;

public interface IAvailabilityListListener extends BaseRecyclerListener<AvailabilityListResponse> {
}
