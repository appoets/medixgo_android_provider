package com.boiddo.android.provider.videocall;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.boiddo.android.provider.R;
import com.boiddo.android.provider.common.utils.CodeSnippet;
import com.boiddo.android.provider.model.dto.response.CheckStatusResponse;
import com.boiddo.android.provider.service.CheckStatusService;
import com.boiddo.android.provider.view.activity.HomeActivity;
import com.boiddo.android.provider.view.activity.twilio.TwilloVideoActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.boiddo.android.provider.view.activity.HomeActivity.currentRequestID;

public class IncomingCallActivity extends AppCompatActivity {

    private static final int ASK_VIDEO_AUDIO_CALL_PERMISSION = 100;

    @BindView(R.id.lblName)
    TextView lblName;
    @BindView(R.id.imgEndCall)
    ImageView imgEndCall;
    @BindView(R.id.imgAcceptCall)
    ImageView imgAcceptCall;
    String chatPath;
    String name;
    String mRequestId;
    Ringtone ringtone;
    private MyReceiver myReceiver;

    @Override
    protected void onStart() {
        super.onStart();

        HomeActivity.show_layout = "true";
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CodeSnippet.USER_ACTION);
        registerReceiver(myReceiver, intentFilter);

    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterReceiver(myReceiver);
        if (isMyServiceRunning(CheckStatusService.class)) {
            stopService(new Intent(this, CheckStatusService.class));
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        setContentView(R.layout.activity_incoming_call);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }

        name = extras.getString("name");
        chatPath = extras.getString("chat_path");
        mRequestId = extras.getString("request_id");
        currentRequestID = Integer.valueOf(mRequestId);
        lblName.setText(name);

        playRingtone();
    }


    @Override
    public void onBackPressed() {

    }

    @OnClick({R.id.imgEndCall, R.id.imgAcceptCall})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgEndCall:

                stopRingtone();
                finish();

                break;
            case R.id.imgAcceptCall:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        openVideoAudioCall();
                    } else {
                        requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA}, ASK_VIDEO_AUDIO_CALL_PERMISSION);
                    }
                } else {
                    openVideoAudioCall();
                }
                break;
        }
    }

    private void openVideoAudioCall() {
        stopRingtone();
        Intent i = new Intent(getApplicationContext(), TwilloVideoActivity.class);
        i.putExtra("chat_path", chatPath);
        i.putExtra("request_id", ""+mRequestId);
        startActivity(i);
    }


    private void playRingtone() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

        if (notification != null) {
            ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
            ringtone.play();
        }
    }


    private void stopRingtone() {
        if (ringtone != null && ringtone.isPlaying())
            ringtone.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isMyServiceRunning(CheckStatusService.class)) {
            Intent intent = new Intent(this, CheckStatusService.class);
            startService(intent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();


        if (isMyServiceRunning(CheckStatusService.class)) {
            stopService(new Intent(this, CheckStatusService.class));
        }

    }

    @Override
    public void finish() {
        super.finish();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }


    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            CheckStatusResponse response;
            if (intent != null && intent.getExtras() != null) {

                if (intent.getParcelableExtra("response") != null) {
                    response = intent.getParcelableExtra("response");
                    Log.e("onReceive Call=>", response.toString());

                    if (response.getServiceStatus().equalsIgnoreCase("active") && response.getRequests() != null && response.getRequests().size() > 0) {
                        currentRequestID = response.getRequests().get(0).getRequestId();
                    }
                }
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ASK_VIDEO_AUDIO_CALL_PERMISSION) {
            if (grantResults.length > 0) {
                boolean permission1 = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean permission2 = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (permission1 && permission2) {
                    openVideoAudioCall();
                } else {
                    Toast.makeText(getApplicationContext(), "Please give permission", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

}
