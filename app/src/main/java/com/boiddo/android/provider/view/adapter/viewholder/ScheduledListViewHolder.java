package com.boiddo.android.provider.view.adapter.viewholder;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.boiddo.android.provider.R;
import com.boiddo.android.provider.ZtoidApplication;
import com.boiddo.android.provider.common.Constants;
import com.boiddo.android.provider.common.utils.CodeSnippet;
import com.boiddo.android.provider.model.dto.response.ScheduledListResponse;
import com.boiddo.android.provider.view.adapter.listener.IScheduledListListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ScheduledListViewHolder extends BaseViewHolder<ScheduledListResponse, IScheduledListListener> {

    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_doctor_name)
    TextView tvDoctorName;
    @BindView(R.id.tv_doctor_spl)
    TextView tvDoctorSpl;
    @BindView(R.id.tv_date_time)
    TextView tvDateTime;

    @BindView(R.id.patient_email)
    TextView patientEmail;
    @BindView(R.id.patient_phoneno)
    TextView patientPhoneno;


    public ScheduledListViewHolder(View itemView, IScheduledListListener listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(ScheduledListResponse data) {
        //ZtoidApplication.getAppContext();
        String url = CodeSnippet.getImageURL(data.getUser().getPicture());
        showImage(ivProfile, url);
        tvDoctorName.setText(data.getUser().getFirstName() +"\t\t"+ data.getUser().getLastName());
        tvDoctorSpl.setText(" - ");
       // patientEmail.setText(data.getUser().getEmail());
        patientPhoneno.setText(data.getUser().getMobile());
        if (data.getScheduleAt() != null && !data.getScheduleAt().equalsIgnoreCase("null"))
            tvDateTime.setText(CodeSnippet.parseDateToyyyyMMdd(data.getScheduleAt()));

       // double latitude = Double.valueOf(data.getUser().getLatitude());
       // double longitude = Double.valueOf(data.getUser().getLongitude());

        /*        Address address = getAddress(latitude ,longitude);
        if (address != null) {
            String streetAddress = getStreetAddress(address);
          //  patientEmail.setText(streetAddress);

        }*/
        patientEmail.setText(data.getUser().getEmail());



    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }


   /*protected String getStreetAddress(Address address) {
       ArrayList<String> addressFragments = new ArrayList<>();
       // Fetch the address lines using getAddressLine,
       // join them, and send them to the thread.
       for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
           addressFragments.add(address.getAddressLine(i));
       }
       String streetAddress = TextUtils.join(", ", addressFragments);
       Log.d(TAG, streetAddress);
       return streetAddress;
   }
   protected Address getAddress(double latitude, double longitude) {
       Address address = null;
       Geocoder geocoder = new Geocoder(ZtoidApplication.getAppContext(), Locale.getDefault());
       try {
           List<Address> addresses = geocoder.getFromLocation(
                   latitude,
                   longitude,
                   1);
           if (addresses != null && !addresses.isEmpty())
               address = addresses.get(0);
       } catch (IOException e) {
           e.printStackTrace();
           Log.e(TAG, e.getMessage());
           //Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
       }
       return address;
   }

    private Location getLastKnownLocation() {
        LocationManager mLocationManager =
                (LocationManager) ZtoidApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers)
            if (ActivityCompat.checkSelfPermission(ZtoidApplication.getAppContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ZtoidApplication.getAppContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) continue;
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy())
                    bestLocation = l;
            }
        if (bestLocation == null) return null;
        return bestLocation;
    }*/
    }

