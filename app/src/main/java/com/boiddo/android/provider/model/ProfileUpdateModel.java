package com.boiddo.android.provider.model;

import com.boiddo.android.provider.model.dto.response.ProfileResponse;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.model.webservice.ApiClient;
import com.boiddo.android.provider.model.webservice.ApiInterface;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ProfileUpdateModel extends BaseModel<ProfileResponse> {

    public ProfileUpdateModel(IModelListener<ProfileResponse> listener) {
        super(listener);
    }

    public void updateProfile(HashMap<String, RequestBody> params, MultipartBody.Part filePart, MultipartBody.Part filePart2) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).postProfileUpdate(params, filePart, filePart2));
    }

    @Override
    public void onSuccessfulApi(ProfileResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
