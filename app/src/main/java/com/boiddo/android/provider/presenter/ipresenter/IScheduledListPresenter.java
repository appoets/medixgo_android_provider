package com.boiddo.android.provider.presenter.ipresenter;

public interface IScheduledListPresenter extends IPresenter {
    void getScheduledList();
}
