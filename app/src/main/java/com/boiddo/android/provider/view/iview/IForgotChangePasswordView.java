package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.presenter.ipresenter.IForgotChangePasswordPresenter;

public interface IForgotChangePasswordView extends IView<IForgotChangePasswordPresenter> {
                void goToLogin();
}
