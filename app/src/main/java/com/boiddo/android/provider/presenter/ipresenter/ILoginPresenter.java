package com.boiddo.android.provider.presenter.ipresenter;

import com.boiddo.android.provider.model.dto.request.LoginRequest;

public interface ILoginPresenter extends IPresenter {
    void postLogin(LoginRequest request);
    void goToRegistration();
    void goToForgotPassword();
}