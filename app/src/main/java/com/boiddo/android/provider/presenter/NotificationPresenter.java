package com.boiddo.android.provider.presenter;

import android.os.Bundle;

import com.boiddo.android.provider.model.CustomException;
import com.boiddo.android.provider.model.NotificationModel;
import com.boiddo.android.provider.model.dto.response.NotificationResponse;
import com.boiddo.android.provider.model.dto.response.Provider;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.presenter.ipresenter.INotificationPresenter;
import com.boiddo.android.provider.view.adapter.NotificationRecyclerAdapter;
import com.boiddo.android.provider.view.iview.INotificationView;

import org.jetbrains.annotations.NotNull;

public class NotificationPresenter extends BasePresenter<INotificationView> implements INotificationPresenter {


    public NotificationPresenter(INotificationView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
    }


    @Override
    public void getNotification() {
        iView.showProgressbar();
        new NotificationModel(new IModelListener<NotificationResponse>() {

            public void onClickItem(int pos, Provider data) {
                iView.showNotificationDialog(pos,data);
            }
            @Override
            public void onSuccessfulApi(@NotNull NotificationResponse response) {
                iView.dismissProgressbar();
                iView.setAdapter(new NotificationRecyclerAdapter(response.getProvider(),this::onClickItem));
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getMissedDetails();
    }
}
