package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.presenter.ipresenter.IChangePasswordPresenter;

public interface IChangePasswordView extends IView<IChangePasswordPresenter> {
                void goToLogin();
}
