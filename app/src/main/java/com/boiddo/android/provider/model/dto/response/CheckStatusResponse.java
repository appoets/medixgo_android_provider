package com.boiddo.android.provider.model.dto.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CheckStatusResponse extends BaseResponse implements Parcelable {

    @SerializedName("account_status")
    @Expose
    private String accountStatus;
    @SerializedName("service_status")
    @Expose
    private String serviceStatus;
    @SerializedName("requests")
    @Expose
    private List<Requests> requests = new ArrayList<>();

    private CheckStatusResponse(Parcel in) {
        accountStatus = in.readString();
        serviceStatus = in.readString();
        requests = in.createTypedArrayList(Requests.CREATOR);
    }

    public final static Creator<CheckStatusResponse> CREATOR = new Creator<CheckStatusResponse>() {
        @Override
        public CheckStatusResponse createFromParcel(Parcel in) {
            return new CheckStatusResponse(in);
        }

        @Override
        public CheckStatusResponse[] newArray(int size) {
            return new CheckStatusResponse[size];
        }
    };

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public List<Requests> getRequests() {
        return requests;
    }

    public void setRequests(List<Requests> requests) {
        this.requests = requests;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(accountStatus);
        dest.writeString(serviceStatus);
        dest.writeTypedList(requests);
    }

    public static class Requests implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("request_id")
        @Expose
        private Integer requestId;
        @SerializedName("provider_id")
        @Expose
        private Integer providerId;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("request")
        @Expose
        private Request request;

        Requests(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            if (in.readByte() == 0) {
                requestId = null;
            } else {
                requestId = in.readInt();
            }
            if (in.readByte() == 0) {
                providerId = null;
            } else {
                providerId = in.readInt();
            }
            if (in.readByte() == 0) {
                status = null;
            } else {
                status = in.readInt();
            }
            request = in.readParcelable(Request.class.getClassLoader());
        }

        static final Creator<Requests> CREATOR = new Creator<Requests>() {
            @Override
            public Requests createFromParcel(Parcel in) {
                return new Requests(in);
            }

            @Override
            public Requests[] newArray(int size) {
                return new Requests[size];
            }
        };

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRequestId() {
            return requestId;
        }

        public void setRequestId(Integer requestId) {
            this.requestId = requestId;
        }

        public Integer getProviderId() {
            return providerId;
        }

        public void setProviderId(Integer providerId) {
            this.providerId = providerId;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Request getRequest() {
            return request;
        }

        public void setRequest(Request request) {
            this.request = request;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (id == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(id);
            }
            if (requestId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(requestId);
            }
            if (providerId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(providerId);
            }
            if (status == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(status);
            }
            dest.writeParcelable(request, flags);
        }
    }

    public static class Request implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("booking_id")
        @Expose
        private String bookingId;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("provider_id")
        @Expose
        private Integer providerId;
        @SerializedName("current_provider_id")
        @Expose
        private Integer currentProviderId;
        @SerializedName("service_type_id")
        @Expose
        private Integer serviceTypeId;
        @SerializedName("before_image")
        @Expose
        private String beforeImage;
        @SerializedName("before_comment")
        @Expose
        private String beforeComment;
        @SerializedName("after_image")
        @Expose
        private String afterImage;
        @SerializedName("after_comment")
        @Expose
        private String afterComment;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("cancelled_by")
        @Expose
        private String cancelledBy;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;
        @SerializedName("paid")
        @Expose
        private Integer paid;
        @SerializedName("distance")
        @Expose
        private Integer distance;
        @SerializedName("s_address")
        @Expose
        private String sAddress;
        @SerializedName("s_latitude")
        @Expose
        private Integer sLatitude;
        @SerializedName("s_longitude")
        @Expose
        private Integer sLongitude;
        @SerializedName("d_address")
        @Expose
        private String dAddress;
        @SerializedName("d_latitude")
        @Expose
        private Integer dLatitude;
        @SerializedName("d_longitude")
        @Expose
        private Integer dLongitude;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("assigned_at")
        @Expose
        private String assignedAt;
        @SerializedName("schedule_at")
        @Expose
        private String scheduleAt;
        @SerializedName("started_at")
        @Expose
        private String startedAt;
        @SerializedName("finished_at")
        @Expose
        private String finishedAt;
        @SerializedName("user_rated")
        @Expose
        private Integer userRated;
        @SerializedName("provider_rated")
        @Expose
        private Integer providerRated;
        @SerializedName("use_wallet")
        @Expose
        private Integer useWallet;
        @SerializedName("call_seconds")
        @Expose
        private Integer callSeconds;
        @SerializedName("broadcast")
        @Expose
        private Integer broadcast;
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("payment")
        @Expose
        private String payment;
        @SerializedName("service_type")
        @Expose
        private ServiceType serviceType;

        Request(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            bookingId = in.readString();
            if (in.readByte() == 0) {
                userId = null;
            } else {
                userId = in.readInt();
            }
            if (in.readByte() == 0) {
                providerId = null;
            } else {
                providerId = in.readInt();
            }
            if (in.readByte() == 0) {
                currentProviderId = null;
            } else {
                currentProviderId = in.readInt();
            }
            if (in.readByte() == 0) {
                serviceTypeId = null;
            } else {
                serviceTypeId = in.readInt();
            }
            beforeImage = in.readString();
            beforeComment = in.readString();
            afterImage = in.readString();
            afterComment = in.readString();
            status = in.readString();
            cancelledBy = in.readString();
            paymentMode = in.readString();
            if (in.readByte() == 0) {
                paid = null;
            } else {
                paid = in.readInt();
            }
            if (in.readByte() == 0) {
                distance = null;
            } else {
                distance = in.readInt();
            }
            sAddress = in.readString();
            if (in.readByte() == 0) {
                sLatitude = null;
            } else {
                sLatitude = in.readInt();
            }
            if (in.readByte() == 0) {
                sLongitude = null;
            } else {
                sLongitude = in.readInt();
            }
            dAddress = in.readString();
            if (in.readByte() == 0) {
                dLatitude = null;
            } else {
                dLatitude = in.readInt();
            }
            if (in.readByte() == 0) {
                dLongitude = null;
            } else {
                dLongitude = in.readInt();
            }
            otp = in.readString();
            assignedAt = in.readString();
            scheduleAt = in.readString();
            startedAt = in.readString();
            finishedAt = in.readString();
            if (in.readByte() == 0) {
                userRated = null;
            } else {
                userRated = in.readInt();
            }
            if (in.readByte() == 0) {
                providerRated = null;
            } else {
                providerRated = in.readInt();
            }
            if (in.readByte() == 0) {
                useWallet = null;
            } else {
                useWallet = in.readInt();
            }
            if (in.readByte() == 0) {
                callSeconds = null;
            } else {
                callSeconds = in.readInt();
            }
            if (in.readByte() == 0) {
                broadcast = null;
            } else {
                broadcast = in.readInt();
            }
            user = in.readParcelable(User.class.getClassLoader());
            payment = in.readString();
            serviceType = in.readParcelable(ServiceType.class.getClassLoader());
        }

        public static final Creator<Request> CREATOR = new Creator<Request>() {
            @Override
            public Request createFromParcel(Parcel in) {
                return new Request(in);
            }

            @Override
            public Request[] newArray(int size) {
                return new Request[size];
            }
        };

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBookingId() {
            return bookingId;
        }

        public void setBookingId(String bookingId) {
            this.bookingId = bookingId;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getProviderId() {
            return providerId;
        }

        public void setProviderId(Integer providerId) {
            this.providerId = providerId;
        }

        public Integer getCurrentProviderId() {
            return currentProviderId;
        }

        public void setCurrentProviderId(Integer currentProviderId) {
            this.currentProviderId = currentProviderId;
        }

        public Integer getServiceTypeId() {
            return serviceTypeId;
        }

        public void setServiceTypeId(Integer serviceTypeId) {
            this.serviceTypeId = serviceTypeId;
        }

        public String getBeforeImage() {
            return beforeImage;
        }

        public void setBeforeImage(String beforeImage) {
            this.beforeImage = beforeImage;
        }

        public String getBeforeComment() {
            return beforeComment;
        }

        public void setBeforeComment(String beforeComment) {
            this.beforeComment = beforeComment;
        }

        public String getAfterImage() {
            return afterImage;
        }

        public void setAfterImage(String afterImage) {
            this.afterImage = afterImage;
        }

        public String getAfterComment() {
            return afterComment;
        }

        public void setAfterComment(String afterComment) {
            this.afterComment = afterComment;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCancelledBy() {
            return cancelledBy;
        }

        public void setCancelledBy(String cancelledBy) {
            this.cancelledBy = cancelledBy;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Integer getPaid() {
            return paid;
        }

        public void setPaid(Integer paid) {
            this.paid = paid;
        }

        public Integer getDistance() {
            return distance;
        }

        public void setDistance(Integer distance) {
            this.distance = distance;
        }

        public String getSAddress() {
            return sAddress;
        }

        public void setSAddress(String sAddress) {
            this.sAddress = sAddress;
        }

        public Integer getSLatitude() {
            return sLatitude;
        }

        public void setSLatitude(Integer sLatitude) {
            this.sLatitude = sLatitude;
        }

        public Integer getSLongitude() {
            return sLongitude;
        }

        public void setSLongitude(Integer sLongitude) {
            this.sLongitude = sLongitude;
        }

        public String getDAddress() {
            return dAddress;
        }

        public void setDAddress(String dAddress) {
            this.dAddress = dAddress;
        }

        public Integer getDLatitude() {
            return dLatitude;
        }

        public void setDLatitude(Integer dLatitude) {
            this.dLatitude = dLatitude;
        }

        public Integer getDLongitude() {
            return dLongitude;
        }

        public void setDLongitude(Integer dLongitude) {
            this.dLongitude = dLongitude;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getAssignedAt() {
            return assignedAt;
        }

        public void setAssignedAt(String assignedAt) {
            this.assignedAt = assignedAt;
        }

        public String getScheduleAt() {
            return scheduleAt;
        }

        public void setScheduleAt(String scheduleAt) {
            this.scheduleAt = scheduleAt;
        }

        public String getStartedAt() {
            return startedAt;
        }

        public void setStartedAt(String startedAt) {
            this.startedAt = startedAt;
        }

        public String getFinishedAt() {
            return finishedAt;
        }

        public void setFinishedAt(String finishedAt) {
            this.finishedAt = finishedAt;
        }

        public Integer getUserRated() {
            return userRated;
        }

        public void setUserRated(Integer userRated) {
            this.userRated = userRated;
        }

        public Integer getProviderRated() {
            return providerRated;
        }

        public void setProviderRated(Integer providerRated) {
            this.providerRated = providerRated;
        }

        public Integer getUseWallet() {
            return useWallet;
        }

        public void setUseWallet(Integer useWallet) {
            this.useWallet = useWallet;
        }

        public Integer getCallSeconds() {
            return callSeconds;
        }

        public void setCallSeconds(Integer callSeconds) {
            this.callSeconds = callSeconds;
        }

        public Integer getBroadcast() {
            return broadcast;
        }

        public void setBroadcast(Integer broadcast) {
            this.broadcast = broadcast;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getPayment() {
            return payment;
        }

        public void setPayment(String payment) {
            this.payment = payment;
        }

        public ServiceType getServiceType() {
            return serviceType;
        }

        public void setServiceType(ServiceType serviceType) {
            this.serviceType = serviceType;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (id == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(id);
            }
            dest.writeString(bookingId);
            if (userId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(userId);
            }
            if (providerId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(providerId);
            }
            if (currentProviderId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(currentProviderId);
            }
            if (serviceTypeId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(serviceTypeId);
            }
            dest.writeString(beforeImage);
            dest.writeString(beforeComment);
            dest.writeString(afterImage);
            dest.writeString(afterComment);
            dest.writeString(status);
            dest.writeString(cancelledBy);
            dest.writeString(paymentMode);
            if (paid == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(paid);
            }
            if (distance == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(distance);
            }
            dest.writeString(sAddress);
            if (sLatitude == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(sLatitude);
            }
            if (sLongitude == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(sLongitude);
            }
            dest.writeString(dAddress);
            if (dLatitude == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(dLatitude);
            }
            if (dLongitude == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(dLongitude);
            }
            dest.writeString(otp);
            dest.writeString(assignedAt);
            dest.writeString(scheduleAt);
            dest.writeString(startedAt);
            dest.writeString(finishedAt);
            if (userRated == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(userRated);
            }
            if (providerRated == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(providerRated);
            }
            if (useWallet == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(useWallet);
            }
            if (callSeconds == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(callSeconds);
            }
            if (broadcast == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(broadcast);
            }
            dest.writeParcelable(user, flags);
            dest.writeString(payment);
            dest.writeParcelable(serviceType, flags);
        }
    }

    public static class ServiceType implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("provider_name")
        @Expose
        private String providerName;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("fixed")
        @Expose
        private Double fixed;
        @SerializedName("price")
        @Expose
        private Double price;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("status")
        @Expose
        private Integer status;

        ServiceType(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            name = in.readString();
            providerName = in.readString();
            image = in.readString();
            if (in.readByte() == 0) {
                fixed = null;
            } else {
                fixed = in.readDouble();
            }
            if (in.readByte() == 0) {
                price = null;
            } else {
                price = in.readDouble();
            }
            description = in.readString();
            if (in.readByte() == 0) {
                status = null;
            } else {
                status = in.readInt();
            }
        }

        public static final Creator<ServiceType> CREATOR = new Creator<ServiceType>() {
            @Override
            public ServiceType createFromParcel(Parcel in) {
                return new ServiceType(in);
            }

            @Override
            public ServiceType[] newArray(int size) {
                return new ServiceType[size];
            }
        };

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProviderName() {
            return providerName;
        }

        public void setProviderName(String providerName) {
            this.providerName = providerName;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Double getFixed() {
            return fixed;
        }

        public void setFixed(Double fixed) {
            this.fixed = fixed;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (id == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(id);
            }
            dest.writeString(name);
            dest.writeString(providerName);
            dest.writeString(image);
            if (fixed == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(fixed);
            }
            if (price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(price);
            }
            dest.writeString(description);
            if (status == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(status);
            }
        }
    }

    public static class User implements Parcelable {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("device_id")
        @Expose
        private String deviceId;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("login_by")
        @Expose
        private String loginBy;
        @SerializedName("social_unique_id")
        @Expose
        private String socialUniqueId;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("stripe_cust_id")
        @Expose
        private String stripeCustId;
        @SerializedName("wallet_balance")
        @Expose
        private Integer walletBalance;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("rating_count")
        @Expose
        private Integer ratingCount;
        @SerializedName("otp")
        @Expose
        private Integer otp;

        User(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            firstName = in.readString();
            lastName = in.readString();
            paymentMode = in.readString();
            email = in.readString();
            picture = in.readString();
            deviceToken = in.readString();
            deviceId = in.readString();
            deviceType = in.readString();
            loginBy = in.readString();
            socialUniqueId = in.readString();
            mobile = in.readString();
            latitude = in.readString();
            longitude = in.readString();
            stripeCustId = in.readString();
            if (in.readByte() == 0) {
                walletBalance = null;
            } else {
                walletBalance = in.readInt();
            }
            rating = in.readString();
            if (in.readByte() == 0) {
                ratingCount = null;
            } else {
                ratingCount = in.readInt();
            }
            if (in.readByte() == 0) {
                otp = null;
            } else {
                otp = in.readInt();
            }
        }

        public static final Creator<User> CREATOR = new Creator<User>() {
            @Override
            public User createFromParcel(Parcel in) {
                return new User(in);
            }

            @Override
            public User[] newArray(int size) {
                return new User[size];
            }
        };

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getLoginBy() {
            return loginBy;
        }

        public void setLoginBy(String loginBy) {
            this.loginBy = loginBy;
        }

        public String getSocialUniqueId() {
            return socialUniqueId;
        }

        public void setSocialUniqueId(String socialUniqueId) {
            this.socialUniqueId = socialUniqueId;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStripeCustId() {
            return stripeCustId;
        }

        public void setStripeCustId(String stripeCustId) {
            this.stripeCustId = stripeCustId;
        }

        public Integer getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(Integer walletBalance) {
            this.walletBalance = walletBalance;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public Integer getRatingCount() {
            return ratingCount;
        }

        public void setRatingCount(Integer ratingCount) {
            this.ratingCount = ratingCount;
        }

        public Integer getOtp() {
            return otp;
        }

        public void setOtp(Integer otp) {
            this.otp = otp;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (id == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(id);
            }
            dest.writeString(firstName);
            dest.writeString(lastName);
            dest.writeString(paymentMode);
            dest.writeString(email);
            dest.writeString(picture);
            dest.writeString(deviceToken);
            dest.writeString(deviceId);
            dest.writeString(deviceType);
            dest.writeString(loginBy);
            dest.writeString(socialUniqueId);
            dest.writeString(mobile);
            dest.writeString(latitude);
            dest.writeString(longitude);
            dest.writeString(stripeCustId);
            if (walletBalance == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(walletBalance);
            }
            dest.writeString(rating);
            if (ratingCount == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(ratingCount);
            }
            if (otp == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(otp);
            }
        }
    }
}
