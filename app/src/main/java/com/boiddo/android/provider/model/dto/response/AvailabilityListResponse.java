package com.boiddo.android.provider.model.dto.response;

import com.google.gson.annotations.SerializedName;

public class AvailabilityListResponse extends BaseResponse {

	@SerializedName("night")
	private int night;

	@SerializedName("available_date")
	private String availableDate;

	@SerializedName("night_start_time")
	private String nightStartTime;

	@SerializedName("night_end_time")
	private String nightEndTime;

	@SerializedName("onsite")
	private int onsite;

	@SerializedName("morning")
	private int morning;

	@SerializedName("morning_end_time")
	private String morningEndTime;

	@SerializedName("afternoon")
	private int afternoon;

	@SerializedName("video_consultancy")
	private int videoConsultancy;

	@SerializedName("afternoon_start_time")
	private String afternoonStartTime;

	@SerializedName("provider_id")
	private String providerId;

	@SerializedName("afternoon_end_time")
	private String afternoonEndTime;

	@SerializedName("id")
	private int id;

	@SerializedName("morning_start_time")
	private String morningStartTime;

	@SerializedName("status")
	private String status;

	public void setNight(int night){
		this.night = night;
	}

	public int getNight(){
		return night;
	}

	public void setAvailableDate(String availableDate){
		this.availableDate = availableDate;
	}

	public String getAvailableDate(){
		return availableDate;
	}

	public void setNightStartTime(String nightStartTime){
		this.nightStartTime = nightStartTime;
	}

	public String getNightStartTime(){
		return nightStartTime;
	}

	public void setNightEndTime(String nightEndTime){
		this.nightEndTime = nightEndTime;
	}

	public String getNightEndTime(){
		return nightEndTime;
	}

	public void setOnsite(int onsite){
		this.onsite = onsite;
	}

	public int getOnsite(){
		return onsite;
	}

	public void setMorning(int morning){
		this.morning = morning;
	}

	public int getMorning(){
		return morning;
	}

	public void setMorningEndTime(String morningEndTime){
		this.morningEndTime = morningEndTime;
	}

	public String getMorningEndTime(){
		return morningEndTime;
	}

	public void setAfternoon(int afternoon){
		this.afternoon = afternoon;
	}

	public int getAfternoon(){
		return afternoon;
	}

	public void setVideoConsultancy(int videoConsultancy){
		this.videoConsultancy = videoConsultancy;
	}

	public int getVideoConsultancy(){
		return videoConsultancy;
	}

	public void setAfternoonStartTime(String afternoonStartTime) {
		this.afternoonStartTime = afternoonStartTime;
	}

	public void setAfternoonEndTime(String afternoonEndTime) {
		this.afternoonEndTime = afternoonEndTime;
	}

	public String getAfternoonStartTime(){
		return afternoonStartTime;
	}

	public void setProviderId(String providerId){
		this.providerId = providerId;
	}

	public String getProviderId(){
		return providerId;
	}



	public String getAfternoonEndTime(){
		return afternoonEndTime;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setMorningStartTime(String morningStartTime){
		this.morningStartTime = morningStartTime;
	}

	public String getMorningStartTime(){
		return morningStartTime;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}