package com.boiddo.android.provider.common;

import java.text.DecimalFormat;

import static com.boiddo.android.provider.BuildConfig.BASE_URL;
import static com.boiddo.android.provider.ZtoidApplication.getApplicationInstance;

public interface Constants {

    interface URL {
        String FCM_URL="https://fcm.googleapis.com/";
        String BASE_URL_STORAGE = BASE_URL+"storage/" ;
    }

    interface BuildFlavour {
        String DEVELOPMENT = "development";
        String PRE_STAGING = "pre_staging";
        String STAGING = "staging";
    }

    interface InternalHttpCode {
        int SUCCESS_CODE = 200;
        int BAD_REQUEST = 400;
        int UNAUTHORIZED_ACCESS = 401;
        int FORBIDDEN = 403;
        int NOT_FOUND = 404;
        int METHOD_NOT_ALLOWED = 405;
        int INTERNAL_SERVER_ERROR = 500;
        int NOT_IMPLEMENTED = 501;
    }

    interface SharedPref {
        String SHARED_PREF = "SHARED_PREF_MCDONALDS";
        String PREF_TOKEN_TYPE = "PREF_TOKEN_TYPE";
        String PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN";
        String PREF_CURRENCY = "PREF_CURRENCY";
        String PREF_OTP = "PREF_OTP";
        String PREF_USER_ID = "PREF_USER_ID";
        String PREF_PROVIDER_PRICE = "PREF_PROVIDER_PRICE";
        String PREF_FCM_TOKEN= "PREF_FCM_TOKEN";
        String PREF_DEVICE_ID= "PREF_DEVICE_ID";
        String PREF_CAT= "PREF_CAT";
    }

    interface HttpErrorMessage {
        String INTERNAL_SERVER_ERROR = "Our server is under maintenance. We will resolve shortly!";
        String METHOD_NOT_ALLOWED_ERROR = "Operational Error!!";
        String UNAUTHORIZED_ACCESS_ERROR = "Your account information was incorrect!";
        String NO_NETWORK_FOUND = "No Network Found";
        String SETTINGS = "Settings";
    }


    interface DecimalValue {
        DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#0.00");
    }

    interface Header {
        String HEADER_CONTENT_NAME = "X-Requested-With";
        String HEADER_CONTENT_VALUE = "XMLHttpRequest";
        String HEADER_AUTHORIZATION = "Authorization";
    }

    interface TimeFormats{
        String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
    }

    interface WebConstants{
        String DEVICE_TYPE = "android";
        String DEVICE_TOKEN = getApplicationInstance().getFCMToken();
        String DEVICE_ID= getApplicationInstance().getDeviceId();
    }

    interface DoctorStatus{
        String ONLINE = "active";
        String OFFLINE = "offline";
    }

    interface MessageConstant{
        String INVOICE = "show_invoice";
    }

    interface BundleConstants{
        String REQUEST_ID ="request_id";
        String CALL_DURATION ="call_duration";
    }

    interface Requests{
        int REQUEST_LOCATION = 1001;
        int REQUEST_IMAGE = 1002;
        int REQUEST_PERMISSION = 1003;
    }

    interface StoragePath{
        String WEB_PATH = BASE_URL +"/storage/";
    }

    interface NotificationConstants{
        String PUSH_NOTIFICATION = "pushNotification";
        int NOTIFICATION_ID = 100;
        int NOTIFICATION_ID_BIG_IMAGE = 101;
    }
}
