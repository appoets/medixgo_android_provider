package com.boiddo.android.provider.view.iview;


import com.boiddo.android.provider.presenter.ipresenter.IChatPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IChatView extends IView<IChatPresenter> {
        void setUp();
}
