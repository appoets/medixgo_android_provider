package com.boiddo.android.provider.model.listener;

import com.boiddo.android.provider.model.CustomException;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IModelListListener<ML> {

    void onSuccessfulApi(@NotNull ML response);

    void onSuccessfulApi(@NotNull List<ML> response);

    void onFailureApi(CustomException e);

    void onUnauthorizedUser(CustomException e);

    void onNetworkFailure();

}
