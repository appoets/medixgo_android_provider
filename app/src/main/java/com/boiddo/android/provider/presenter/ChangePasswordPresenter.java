package com.boiddo.android.provider.presenter;

import com.boiddo.android.provider.model.ChangePasswordModel;
import com.boiddo.android.provider.model.CustomException;
import com.boiddo.android.provider.model.dto.request.PasswordRequest;
import com.boiddo.android.provider.model.dto.response.BaseResponse;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.presenter.ipresenter.IChangePasswordPresenter;
import com.boiddo.android.provider.view.iview.IChangePasswordView;

import org.jetbrains.annotations.NotNull;


public class ChangePasswordPresenter extends BasePresenter<IChangePasswordView> implements IChangePasswordPresenter {

    public ChangePasswordPresenter(IChangePasswordView iView) {
        super(iView);
    }


    @Override
    public void updatePassword(PasswordRequest request) {
            iView.showProgressbar();
            new ChangePasswordModel(new IModelListener<BaseResponse>() {
                @Override
                public void onSuccessfulApi(@NotNull BaseResponse response) {
                    iView.dismissProgressbar();
                    iView.goToLogin();
                }

                @Override
                public void onFailureApi(CustomException e) {
                    iView.dismissProgressbar();
                    iView.showSnackBar(e.getMessage());
                }

                @Override
                public void onUnauthorizedUser(CustomException e) {
                    iView.dismissProgressbar();
                    iView.showSnackBar(e.getMessage());
                }

                @Override
                public void onNetworkFailure() {
                    iView.showNetworkMessage();
                    iView.dismissProgressbar();
                }
            }).updatePassword(request);
    }

    @Override
    public void goToLogin() {
        iView.goToLogin();
    }
}
