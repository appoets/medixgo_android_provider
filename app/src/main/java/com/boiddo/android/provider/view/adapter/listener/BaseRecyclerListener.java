package com.boiddo.android.provider.view.adapter.listener;

public interface BaseRecyclerListener<BRL> {

    void onClickItem(int pos, BRL data);

}
