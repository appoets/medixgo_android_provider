package com.boiddo.android.provider.view.adapter.listener;


import com.boiddo.android.provider.model.dto.response.ScheduledListResponse;

public interface IScheduledListListener extends BaseRecyclerListener<ScheduledListResponse> {
}
