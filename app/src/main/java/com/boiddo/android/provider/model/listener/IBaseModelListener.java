package com.boiddo.android.provider.model.listener;


import com.boiddo.android.provider.model.CustomException;

public interface IBaseModelListener<BML> {

    void onSuccessfulApi(BML response);

    void onFailureApi(CustomException e);

    void onUnauthorizedUser(CustomException e);

    void onNetworkFailure();
}
