package com.boiddo.android.provider.presenter;

import com.boiddo.android.provider.presenter.ipresenter.IScheduleDialogPresenter;
import com.boiddo.android.provider.view.iview.IScheduleDialogView;


public class ScheduleDialogPresenter extends BasePresenter<IScheduleDialogView> implements IScheduleDialogPresenter {


    public ScheduleDialogPresenter(IScheduleDialogView iView) {
        super(iView);
    }

}
