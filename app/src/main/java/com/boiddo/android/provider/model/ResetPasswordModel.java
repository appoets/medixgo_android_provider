package com.boiddo.android.provider.model;


import com.boiddo.android.provider.model.dto.request.ResetPasswordRequest;
import com.boiddo.android.provider.model.dto.response.BaseResponse;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.model.webservice.ApiClient;
import com.boiddo.android.provider.model.webservice.ApiInterface;

public class ResetPasswordModel extends BaseModel<BaseResponse> {

    public ResetPasswordModel(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    public void resetPassword(ResetPasswordRequest request) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).resetPassword(request));
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
