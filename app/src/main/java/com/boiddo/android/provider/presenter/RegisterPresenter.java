package com.boiddo.android.provider.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.boiddo.android.provider.common.Constants;
import com.boiddo.android.provider.model.CustomException;
import com.boiddo.android.provider.model.LoginModel;
import com.boiddo.android.provider.model.RegisterModel;
import com.boiddo.android.provider.model.ServiceModel;
import com.boiddo.android.provider.model.dto.request.LoginRequest;
import com.boiddo.android.provider.model.dto.request.RegisterRequest;
import com.boiddo.android.provider.model.dto.response.LoginResponse;
import com.boiddo.android.provider.model.dto.response.RegisterResponse;
import com.boiddo.android.provider.model.dto.response.ServiceResponse;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.presenter.ipresenter.IRegisterPresenter;
import com.boiddo.android.provider.view.iview.IRegisterView;

import org.jetbrains.annotations.NotNull;

import static com.boiddo.android.provider.ZtoidApplication.getApplicationInstance;


public class RegisterPresenter extends BasePresenter<IRegisterView> implements IRegisterPresenter {

    @Override
    public void goToLogin() {
        iView.goToLogin();
    }

    public RegisterPresenter(IRegisterView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getServiceData();
    }

    @Override
    public void getServiceData() {
        new ServiceModel(new IModelListener<ServiceResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ServiceResponse response) {
                iView.dismissProgressbar();
                iView.setUpData(response.getService());
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getServiceDetails();
    }


    @Override
    public void postRegister(RegisterRequest registerRequest) {
        iView.showProgressbar();
        new RegisterModel(new IModelListener<RegisterResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull RegisterResponse response) {
                LoginRequest request = new LoginRequest();
                request.setEmail(registerRequest.getEmail());
                request.setPassword(registerRequest.getPassword());
                request.setDevice_id(registerRequest.getDevice_id());
                request.setDevice_token(registerRequest.getDevice_token());
                request.setDevice_type(Constants.WebConstants.DEVICE_TYPE);
                postLogin(request);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).postRegister(registerRequest);
    }

    @Override
    public void postLogin(LoginRequest request) {
        new LoginModel(new IModelListener<LoginResponse>() {
            @Override
            public void onSuccessfulApi(@NonNull LoginResponse response) {
                iView.dismissProgressbar();
                getApplicationInstance().setAccessToken(response.getAccessToken());
                iView.goToHome();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).postLogin(request);
    }
}
