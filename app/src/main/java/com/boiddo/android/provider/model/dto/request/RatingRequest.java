package com.boiddo.android.provider.model.dto.request;


public class RatingRequest {

  private String rating="";
  private String command="";

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
