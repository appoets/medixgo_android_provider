package com.boiddo.android.provider.view.adapter.listener;

import com.boiddo.android.provider.model.dto.common.HistoryItem;

public interface IHistoryRecyclerListener extends BaseRecyclerListener<HistoryItem> {
}
