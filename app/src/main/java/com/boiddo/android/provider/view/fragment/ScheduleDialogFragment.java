package com.boiddo.android.provider.view.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.philliphsu.bottomsheetpickers.date.DatePickerDialog;
import com.philliphsu.bottomsheetpickers.time.BottomSheetTimePickerDialog;
import com.philliphsu.bottomsheetpickers.time.grid.GridTimePickerDialog;
import com.boiddo.android.provider.R;
import com.boiddo.android.provider.common.Constants;
import com.boiddo.android.provider.common.utils.CodeSnippet;
import com.boiddo.android.provider.model.dto.request.ScheduleRequest;
import com.boiddo.android.provider.model.dto.response.Provider;
import com.boiddo.android.provider.presenter.SchedulePresenter;
import com.boiddo.android.provider.view.iview.IScheduleView;
import com.boiddo.android.provider.view.widget.CustomProgressbar;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

import static java.lang.String.format;


/**
 * <p>A fragment that shows a list of items as a modal bottom sheet.</p>
 * <p>You can show this modal bottom sheet from your activity like this:</p>
 * <pre>
 *     ScheduleDialogFragment.newInstance().show(getSupportFragmentManager(), "dialog");
 * </pre>
 */
public class ScheduleDialogFragment extends BottomSheetDialogFragment implements com.philliphsu.bottomsheetpickers.date.DatePickerDialog.OnDateSetListener, BottomSheetTimePickerDialog.OnTimeSetListener,IScheduleView {

    @Nullable
    @BindView(R.id.rlDate)
    RelativeLayout rlDate;

    @Nullable
    @BindView(R.id.rlTime)
    RelativeLayout rlTime;

    @Nullable
    @BindView(R.id.tvDateVal)
    TextView tvDateVal;

    @Nullable
    @BindView(R.id.tvTimeVal)
    TextView tvTimeVal;

    @Nullable
    @BindView(R.id.tvSchedule)
    TextView tvSchedule;

    private SchedulePresenter iPresenter;
    private Provider received_data;
    private String date ="";
    private String time ="";

    protected CustomProgressbar mCustomProgressbar;

    // TODO: Customize parameters
    public static ScheduleDialogFragment newInstance(Provider data) {
        ScheduleDialogFragment fragment = new ScheduleDialogFragment();
        Bundle extras = new Bundle();
        extras.putSerializable("provider_data",data);
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        received_data = (Provider) bundle.getSerializable("provider_data");
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedule_dialog, container);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        iPresenter = new SchedulePresenter(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Optional
    @OnClick({R.id.rlDate, R.id.rlTime,R.id.tvSchedule})
    public void OnViewClicked(View view) {

        switch (view.getId()) {
            case R.id.rlDate:
               iPresenter.showCalendarDialog();
                break;

            case R.id.rlTime:
                iPresenter.showTimePickerDialog();
                break;

            case R.id.tvSchedule:
                if (TextUtils.isEmpty(date)){
                    showToast(getString(R.string.select_date_schedule));
                }else  if (TextUtils.isEmpty(time)){
                    showToast(getString(R.string.select_time_schedule));
                }else{
                    ScheduleRequest request = new ScheduleRequest();
                    request.setProvider_id(received_data.getProviderId()+"");
                    request.setUser_id(received_data.getUserId()+"");
                    request.setService_type(received_data.getServiceTypeId()+"");
                    request.setSchedule_date(date+" "+time);
                    iPresenter.updateSchedule(request);
                }
                break;
        }
    }

    private CustomProgressbar getProgressBar() {
        if (mCustomProgressbar == null) mCustomProgressbar = new CustomProgressbar(getContext());
        return mCustomProgressbar;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        @SuppressLint("DefaultLocale") String formattedDate = format("%s-%s-%d",format("%02d", dayOfMonth) , format("%02d", monthOfYear), year);
        date = format("%d-%s-%s" ,year , format("%02d", monthOfYear),format("%02d", dayOfMonth));
        assert tvDateVal != null;
        tvDateVal.setText(formattedDate);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onTimeSet(ViewGroup viewGroup, int hourOfDay, int minute) {
        Calendar cal = new java.util.GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        assert tvTimeVal != null;
        tvTimeVal.setText(DateFormat.getTimeFormat(getContext()).format(cal.getTime()));
        time = format("%s:%s:%s",format("%02d",hourOfDay),format("%02d",minute),"00");
    }


    @Override
    public void goToNotificationScreen(String message) {
        showToast(message);
        Dialog dialog1 = getDialog();
        dialog1.dismiss();
    }

    @Override
    public void showCalendarDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog date = DatePickerDialog.newInstance(
                ScheduleDialogFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        assert getFragmentManager() != null;
        date.show(getFragmentManager(), "CalendarDialog");
    }

    @Override
    public void showTimeDialog() {
        Calendar now = Calendar.getInstance();
        GridTimePickerDialog grid = GridTimePickerDialog.newInstance(
                ScheduleDialogFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false);
        assert getFragmentManager() != null;
        grid.show(getFragmentManager(), "TimerDialog");
    }

    @Override
    public void showProgressbar() {
        try {
            getProgressBar().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismissProgressbar() {
        getActivity().runOnUiThread(() -> {
            try {
                getProgressBar().dismissProgress();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(getView(), message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();
    }

    @Override
    public void showNetworkMessage() {
            Snackbar snackbar = Snackbar.make(getView(), Constants.HttpErrorMessage.NO_NETWORK_FOUND, Snackbar.LENGTH_INDEFINITE);
            snackbar.setActionTextColor(Color.RED);
            snackbar.setAction(Constants.HttpErrorMessage.SETTINGS, view -> new CodeSnippet().showNetworkSettings());
            snackbar.show();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
