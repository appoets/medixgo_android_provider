package com.boiddo.android.provider.presenter.ipresenter;

import com.boiddo.android.provider.model.dto.request.ScheduleRequest;

/**
 * Created by Tranxit Technologies.
 */

public interface ISchedulePresenter {
    void updateSchedule(ScheduleRequest request);
    void showCalendarDialog();
    void showTimePickerDialog();
}
