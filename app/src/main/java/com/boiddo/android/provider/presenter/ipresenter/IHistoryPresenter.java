package com.boiddo.android.provider.presenter.ipresenter;

public interface IHistoryPresenter extends IPresenter {
        void getHistory();
}