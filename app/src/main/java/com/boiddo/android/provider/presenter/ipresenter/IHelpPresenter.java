package com.boiddo.android.provider.presenter.ipresenter;

public interface IHelpPresenter extends IPresenter {
    void getHelpDetails();
}