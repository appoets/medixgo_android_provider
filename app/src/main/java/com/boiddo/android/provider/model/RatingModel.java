package com.boiddo.android.provider.model;

import com.boiddo.android.provider.model.dto.request.RatingRequest;
import com.boiddo.android.provider.model.dto.response.BaseResponse;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.model.webservice.ApiClient;
import com.boiddo.android.provider.model.webservice.ApiInterface;

public class RatingModel extends BaseModel<BaseResponse> {

    public RatingModel(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void updateRating(String id,RatingRequest request) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).updateRating(id,request));
    }
}
