package com.boiddo.android.provider.view.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.google.firebase.iid.FirebaseInstanceId;
import com.boiddo.android.provider.R;
import com.boiddo.android.provider.model.dto.common.HistoryItem;
import com.boiddo.android.provider.presenter.HistoryPresenter;
import com.boiddo.android.provider.presenter.ipresenter.IHistoryPresenter;
import com.boiddo.android.provider.view.adapter.HistoryAdapter;
import com.boiddo.android.provider.view.iview.IHistoryView;

import butterknife.BindView;
import butterknife.OnClick;

public class HistoryActivity extends BaseActivity<IHistoryPresenter> implements IHistoryView {

    private static final String TAG = "HistoryActivity";
    @BindView(R.id.ibBack)
    ImageButton ibBack;

    @BindView(R.id.rcvHistory)
    RecyclerView rcvHistory;
    @BindView(R.id.llNoHistory)
    LinearLayout llNoHistory;


    @Override
    int attachLayout() {
        return R.layout.activity_history;
    }

    @Override
    IHistoryPresenter initialize() {
        return new HistoryPresenter(this);
    }

    @Override
    public void initSetUp() {
        rcvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG, "Token Received ==>> " + token);
    }


    @OnClick({R.id.ibBack})
    public void OnViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
        }
    }


    @Override
    public void setAdapter(HistoryAdapter adapter) {
        if (adapter.getItemCount() > 0) {
            llNoHistory.setVisibility(View.GONE);
            rcvHistory.setVisibility(View.VISIBLE);
            rcvHistory.setAdapter(adapter);
        } else {
            rcvHistory.setVisibility(View.GONE);
            llNoHistory.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void moveToChat(HistoryItem data) {
        Intent intent1 = new Intent(getActivity(), ChatActivity.class);
        intent1.putExtra("ct_user_id", data.getUserId());
        intent1.putExtra("sender_id", data.getProviderId());
        intent1.putExtra("sender_name", data.getUser().getFirstName() + " " + data.getUser().getLastName());
        startActivity(intent1);
    }
}
