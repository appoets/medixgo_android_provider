package com.boiddo.android.provider.presenter;

import android.os.Bundle;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.boiddo.android.provider.model.CustomException;
import com.boiddo.android.provider.model.ProfileModel;
import com.boiddo.android.provider.model.ProfileUpdateModel;
import com.boiddo.android.provider.model.ServiceModel;
import com.boiddo.android.provider.model.dto.common.ServiceItem;
import com.boiddo.android.provider.model.dto.response.ProfileResponse;
import com.boiddo.android.provider.model.dto.response.ServiceResponse;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.presenter.ipresenter.IProfilePresenter;
import com.boiddo.android.provider.view.iview.IProfileView;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.boiddo.android.provider.ZtoidApplication.getApplicationInstance;

public class ProfilePresenter extends BasePresenter<IProfileView> implements IProfilePresenter {

    public ProfilePresenter(IProfileView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getUserDetails();
        getServiceData();
    }

    private void getUserDetails() {
        iView.showProgressbar();
        new ProfileModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                iView.dismissProgressbar();
                getApplicationInstance().setCurrency(response.getCurrency());
                iView.updateUserDetails(response);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getUserDetails();
    }

    @Override
    public void getServiceData() {
        new ServiceModel(new IModelListener<ServiceResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ServiceResponse response) {
                iView.dismissProgressbar();
                iView.setSpecialitiesList(response.getService());
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getServiceDetails();
    }

    @Override
    public void setSpecialitiesName(int specialities_id, List<ServiceItem> serviceItemList) {
        List<ServiceItem> listSelected = Stream.of(serviceItemList).filter(p->(p.getId()==specialities_id)).collect(Collectors.toList());
        iView.setSpecialityName(listSelected.get(0).getName());
    }


    @Override
    public void updateProfile(HashMap<String, RequestBody> params, MultipartBody.Part filePart, MultipartBody.Part filePart2) {
        iView.showProgressbar();
        new ProfileUpdateModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                iView.dismissProgressbar();
                getUserDetails();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).updateProfile(params,filePart, filePart2);
    }

    @Override
    public void goToChangePassword() {
        iView.goToChangePassword();
    }
}
