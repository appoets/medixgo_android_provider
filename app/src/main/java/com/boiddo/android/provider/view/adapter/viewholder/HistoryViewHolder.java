package com.boiddo.android.provider.view.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.boiddo.android.provider.R;
import com.boiddo.android.provider.common.utils.CodeSnippet;
import com.boiddo.android.provider.model.dto.common.HistoryItem;
import com.boiddo.android.provider.model.dto.common.User;
import com.boiddo.android.provider.view.adapter.listener.IHistoryRecyclerListener;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class HistoryViewHolder extends BaseViewHolder<HistoryItem, IHistoryRecyclerListener> {

    @BindView(R.id.civProfile)
    CircleImageView civProfile;

    @BindView(R.id.tvPatientName)
    TextView tvPatientName;
    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.ivChat)
    ImageView ivChat;

    public HistoryViewHolder(View itemView, IHistoryRecyclerListener listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(HistoryItem data) {
            if (data.getUser()!=null){
                User user = data.getUser();
                tvPatientName.setText(user.getFirstName()+" "+user.getLastName());
            }
        tvDate.setText(CodeSnippet.parseDateToyyyyMMdd(data.getAssignedAt()));
    }

    @OnClick({R.id.ivChat})
    public void OnClick(View view){
        switch (view.getId()){
            case R.id.ivChat:
                listener.onClickItem(getAdapterPosition(),data);
                break;
        }
    }
}
