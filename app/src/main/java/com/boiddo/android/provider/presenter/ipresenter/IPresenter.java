package com.boiddo.android.provider.presenter.ipresenter;

import android.content.Intent;
import android.os.Bundle;

import com.boiddo.android.provider.model.CustomException;


public interface IPresenter {

    void onCreate(Bundle bundle);

    void onStart();

    void onResume();

    void onStop();

    void onPause();

    void onDestroy();

    void onActivityForResult(int requestCode, int resultCode, Intent data);

    String getStringRes(int resId);

    void onLogout(CustomException e);

    void onLogout();

}
