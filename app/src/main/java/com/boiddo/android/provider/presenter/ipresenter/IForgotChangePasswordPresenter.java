package com.boiddo.android.provider.presenter.ipresenter;

import com.boiddo.android.provider.model.dto.request.ResetPasswordRequest;

public interface IForgotChangePasswordPresenter extends IPresenter {
    void goToLogin();
    void resetPassword(ResetPasswordRequest resetPasswordRequest);
}