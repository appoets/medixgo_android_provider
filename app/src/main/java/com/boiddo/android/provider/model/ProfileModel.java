package com.boiddo.android.provider.model;


import com.boiddo.android.provider.model.dto.response.ProfileResponse;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.model.webservice.ApiClient;
import com.boiddo.android.provider.model.webservice.ApiInterface;

public class ProfileModel extends BaseModel<ProfileResponse> {

    public ProfileModel(IModelListener<ProfileResponse> listener) {
        super(listener);
    }

    public void getUserDetails() {
       enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getUserDetails());
    }

    @Override
    public void onSuccessfulApi(ProfileResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
