package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.model.dto.response.HelpResponse;
import com.boiddo.android.provider.presenter.ipresenter.IHelpPresenter;

public interface IHelpView extends IView<IHelpPresenter> {
        void updateHelpDetails(HelpResponse response);
}
