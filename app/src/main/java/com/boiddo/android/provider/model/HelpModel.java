package com.boiddo.android.provider.model;

import com.boiddo.android.provider.model.dto.response.HelpResponse;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.model.webservice.ApiClient;
import com.boiddo.android.provider.model.webservice.ApiInterface;

public class HelpModel extends BaseModel<HelpResponse> {

    public HelpModel(IModelListener<HelpResponse> listener) {
        super(listener);
    }

    public void getHelpDetails(){
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getHelpDetails());
    }

    @Override
    public void onSuccessfulApi(HelpResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
