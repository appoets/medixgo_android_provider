package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.model.dto.common.ServiceItem;
import com.boiddo.android.provider.presenter.ipresenter.IRegisterPresenter;

import java.util.List;

public interface IRegisterView extends IView<IRegisterPresenter> {
    void goToLogin();
    void goToHome();
    void setUpData(List<ServiceItem> itemList);
}
