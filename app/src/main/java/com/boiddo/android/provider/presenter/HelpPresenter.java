package com.boiddo.android.provider.presenter;

import android.os.Bundle;

import com.boiddo.android.provider.model.CustomException;
import com.boiddo.android.provider.model.HelpModel;
import com.boiddo.android.provider.model.dto.response.HelpResponse;
import com.boiddo.android.provider.model.listener.IModelListener;
import com.boiddo.android.provider.presenter.ipresenter.IHelpPresenter;
import com.boiddo.android.provider.view.iview.IHelpView;

import org.jetbrains.annotations.NotNull;

public class HelpPresenter extends BasePresenter<IHelpView> implements IHelpPresenter {

    public HelpPresenter(IHelpView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getHelpDetails();
    }

    @Override
    public void getHelpDetails() {
        new HelpModel(new IModelListener<HelpResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull HelpResponse response) {
                iView.updateHelpDetails(response);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getHelpDetails();
    }
}
