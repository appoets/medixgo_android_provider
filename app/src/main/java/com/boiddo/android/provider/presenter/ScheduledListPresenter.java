package com.boiddo.android.provider.presenter;

import android.os.Bundle;

import com.boiddo.android.provider.model.CustomException;
import com.boiddo.android.provider.model.ScheduledListModel;
import com.boiddo.android.provider.model.dto.response.ScheduledListResponse;
import com.boiddo.android.provider.model.listener.IModelListListener;
import com.boiddo.android.provider.presenter.ipresenter.IScheduledListPresenter;
import com.boiddo.android.provider.view.adapter.listener.IScheduledListListener;
import com.boiddo.android.provider.view.iview.IScheduledListView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.boiddo.android.provider.ZtoidApplication.getApplicationInstance;

public class ScheduledListPresenter extends BasePresenter<IScheduledListView> implements IScheduledListPresenter {

    public ScheduledListPresenter(IScheduledListView iView) {
        super(iView);
        getScheduledList();
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
    }

    IScheduledListListener iScheduledListListener = new IScheduledListListener() {
        @Override
        public void onClickItem(int pos, ScheduledListResponse data) {

            getApplicationInstance().setCategory(data.getService_type().getProviderName());
            iView.moveToDetail(data);
        }
    };

    @Override
    public void getScheduledList() {
        iView.showProgressbar();
        new ScheduledListModel(new IModelListListener<ScheduledListResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ScheduledListResponse response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull List<ScheduledListResponse> response) {
                iView.dismissProgressbar();
                iView.setAdapter(response, iScheduledListListener);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getScheduleList();
    }
}
