package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.model.dto.response.Provider;
import com.boiddo.android.provider.presenter.ipresenter.INotificationPresenter;
import com.boiddo.android.provider.view.adapter.NotificationRecyclerAdapter;

public interface INotificationView extends IView<INotificationPresenter> {
    void initSetUp();
    void setAdapter(NotificationRecyclerAdapter adapter);
    void showNotificationDialog(int pos, Provider data);
}
