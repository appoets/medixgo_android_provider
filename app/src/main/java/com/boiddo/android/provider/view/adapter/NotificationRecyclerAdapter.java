package com.boiddo.android.provider.view.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.boiddo.android.provider.R;
import com.boiddo.android.provider.model.dto.response.Provider;
import com.boiddo.android.provider.view.adapter.listener.INotificationRecyclerAdapter;
import com.boiddo.android.provider.view.adapter.viewholder.NotificationViewHolder;


import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class NotificationRecyclerAdapter extends BaseRecyclerAdapter<INotificationRecyclerAdapter, Provider, NotificationViewHolder> {

    List<Provider> notificationResponseList;
    INotificationRecyclerAdapter iNotificationRecyclerAdapter;

    public NotificationRecyclerAdapter(List<Provider> notificationResponseList, INotificationRecyclerAdapter iNotificationRecyclerAdapter) {
        super(notificationResponseList, iNotificationRecyclerAdapter);
        this.notificationResponseList = notificationResponseList;
        this.iNotificationRecyclerAdapter = iNotificationRecyclerAdapter;

    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotificationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false), iNotificationRecyclerAdapter);
    }
}
