package com.boiddo.android.provider.view.iview;

import com.boiddo.android.provider.model.dto.common.ServiceItem;
import com.boiddo.android.provider.model.dto.response.ProfileResponse;
import com.boiddo.android.provider.presenter.ipresenter.IProfilePresenter;

import java.util.List;

public interface IProfileView extends IView<IProfilePresenter> {
    void updateUserDetails(ProfileResponse response);
    void setSpecialitiesList(List<ServiceItem> itemList);
    void setSpecialityName(String name);
    void goToChangePassword();
}
