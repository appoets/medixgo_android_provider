
package com.boiddo.android.provider.model.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvoiceResponse extends BaseResponse {

    @SerializedName("id")
    @Expose
    private Double id;
    @SerializedName("request_id")
    @Expose
    private Double requestId;
    @SerializedName("promocode_id")
    @Expose
    private String promocodeId;
    @SerializedName("payment_id")
    @Expose
    private String paymentId;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("fixed")
    @Expose
    private Double fixed;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("commision")
    @Expose
    private Double commision;
    @SerializedName("time_price")
    @Expose
    private Double timePrice;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("tax")
    @Expose
    private Double tax;
    @SerializedName("wallet")
    @Expose
    private Double wallet;
    @SerializedName("total")
    @Expose
    private Double total;

    public Double getId() {
        return id;
    }

    public void setId(Double id) {
        this.id = id;
    }

    public Double getRequestId() {
        return requestId;
    }

    public void setRequestId(Double requestId) {
        this.requestId = requestId;
    }

    public String getPromocodeId() {
        return promocodeId;
    }

    public void setPromocodeId(String promocodeId) {
        this.promocodeId = promocodeId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Double getFixed() {
        return fixed;
    }

    public void setFixed(Double fixed) {
        this.fixed = fixed;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getCommision() {
        return commision;
    }

    public void setCommision(Double commision) {
        this.commision = commision;
    }

    public Double getTimePrice() {
        return timePrice;
    }

    public void setTimePrice(Double timePrice) {
        this.timePrice = timePrice;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getWallet() {
        return wallet;
    }

    public void setWallet(Double wallet) {
        this.wallet = wallet;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
