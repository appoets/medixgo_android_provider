package com.boiddo.android.provider.model;

import android.support.annotation.NonNull;

import com.boiddo.android.provider.common.Constants;
import com.boiddo.android.provider.common.utils.CodeSnippet;
import com.boiddo.android.provider.model.dto.response.BaseResponse;
import com.boiddo.android.provider.model.listener.IBaseListModelListener;
import com.boiddo.android.provider.model.listener.IModelListListener;
import com.boiddo.android.provider.model.webservice.ApiClient;

import java.lang.annotation.Annotation;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public abstract class BaseListModel<BM extends BaseResponse> implements IBaseListModelListener<BM> {

    IModelListListener<BM> listener;

    protected String TAG = getClass().getSimpleName();

    private Callback<BM> baseModelCallBack = new Callback<BM>() {
        @Override
        public void onResponse(@NonNull Call<BM> call, @NonNull Response<BM> response) {
            try {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.code() == Constants.InternalHttpCode.SUCCESS_CODE)
                        onSuccessfulApi(response.body());
                    else
                        onFailureApi(new CustomException(response.code(), response.message()));
                } else if (response.body() != null)
                    onFailureApi(new CustomException(response.code(), response.message()));
                else if (response.errorBody() != null) {
                    try {
                        Converter<ResponseBody, BM> converter =
                                new ApiClient().getClient().responseBodyConverter(BaseResponse.class, new Annotation[0]);
                        CustomException exception =
                                new CustomException(response.code(), converter.convert(response.errorBody()));
                        if (response.code() == Constants.InternalHttpCode.UNAUTHORIZED_ACCESS) {
                            onUnauthorizedUser(exception);
                            return;
                        }
                        onFailureApi(exception);
                    } catch (Exception e) {
                        e.printStackTrace();
                        onFailureApi(new CustomException(response.code(), Constants.HttpErrorMessage.INTERNAL_SERVER_ERROR));
                    }
                } else
                    onFailureApi(new CustomException(response.code(), Constants.HttpErrorMessage.INTERNAL_SERVER_ERROR));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(@NonNull Call<BM> call, @NonNull Throwable t) {
            onFailureApi(new CustomException(Constants.InternalHttpCode.NOT_IMPLEMENTED, t.getLocalizedMessage()));
        }
    };
    private Callback<List<BM>> baseModelCallBack1 = new Callback<List<BM>>() {
        @Override
        public void onResponse(@NonNull Call<List<BM>> call, @NonNull Response<List<BM>> response) {
            try {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.code() == Constants.InternalHttpCode.SUCCESS_CODE)
                        onSuccessfulApi(response.body());
                    else
                        onFailureApi(new CustomException(response.code(), response.message()));
                } else if (response.body() != null)
                    onFailureApi(new CustomException(response.code(), response.message()));
                else if (response.errorBody() != null) {
                    try {
                        Converter<ResponseBody, BM> converter =
                                new ApiClient().getClient().responseBodyConverter(BaseResponse.class, new Annotation[0]);
                        CustomException exception =
                                new CustomException(response.code(), converter.convert(response.errorBody()));
                        if (response.code() == Constants.InternalHttpCode.UNAUTHORIZED_ACCESS) {
                            onUnauthorizedUser(exception);
                            return;
                        }
                        onFailureApi(exception);
                    } catch (Exception e) {
                        e.printStackTrace();
                        onFailureApi(new CustomException(response.code(), Constants.HttpErrorMessage.INTERNAL_SERVER_ERROR));
                    }
                } else
                    onFailureApi(new CustomException(response.code(), Constants.HttpErrorMessage.INTERNAL_SERVER_ERROR));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(@NonNull Call<List<BM>> call, @NonNull Throwable t) {
            onFailureApi(new CustomException(Constants.InternalHttpCode.NOT_IMPLEMENTED, t.getLocalizedMessage()));
        }
    };


    BaseListModel(IModelListListener<BM> listener) {
        this.listener = listener;
    }


    void enQueueTask(Call<BM> tCall) {
        if (new CodeSnippet().hasNetwork()) tCall.enqueue(baseModelCallBack);
        else onNetworkFailure();
    }

    void enQueueListTask(Call<List<BM>> tCall) {
        if (new CodeSnippet().hasNetwork()) tCall.enqueue(baseModelCallBack1);
        else onNetworkFailure();
    }

}
