package com.boiddo.android.provider.model.listener;



import com.boiddo.android.provider.model.CustomException;

import org.jetbrains.annotations.NotNull;

public interface IModelListener<ML> {

    void onSuccessfulApi(@NotNull ML response);

    void onFailureApi(CustomException e);

    void onUnauthorizedUser(CustomException e);

    void onNetworkFailure();
}
