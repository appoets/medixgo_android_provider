package com.boiddo.android.provider.view.adapter.listener;

import com.boiddo.android.provider.model.dto.response.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface INotificationRecyclerAdapter extends BaseRecyclerListener<Provider> {
}
