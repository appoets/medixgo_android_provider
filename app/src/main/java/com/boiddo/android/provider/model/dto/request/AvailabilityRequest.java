package com.boiddo.android.provider.model.dto.request;

public class AvailabilityRequest {
    private String service_status;

    public String getService_status() {
        return service_status;
    }

    public void setService_status(String service_status) {
        this.service_status = service_status;
    }
}
