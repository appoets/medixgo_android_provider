package com.boiddo.android.provider.presenter.ipresenter;

import com.boiddo.android.provider.model.dto.request.PasswordRequest;

public interface IChangePasswordPresenter extends IPresenter {
    void updatePassword(PasswordRequest request);
    void goToLogin();
}